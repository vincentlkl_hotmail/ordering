angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function($http) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  // var friends = [
  //   { id: 0, name: 'Food 1', description: "Description for food 1" },
  //   { id: 1, name: 'Food 2' , description: "Decription for food 2" },
  //   { id: 2, name: 'Food 3', description: "Description for food 3" },
  //   { id: 3, name: 'Food 4' , description: "Decription for food 2" },
  //   { id: 4, name: 'Food 5', description: "Description for food 4" },
  //   { id: 5, name: 'Food 6' , description: "Decription for food 5" },
  //   { id: 6, name: 'Food 7', description: "Description for food 6" },
  //   { id: 7, name: 'Food 8' , description: "Decription for food 7" },
  //   { id: 8, name: 'Food 9', description: "Description for food 8" },
  //   { id: 9, name: 'Food 10' , description: "Decription for food 9" },
  //   { id: 10, name: 'Food 11', description: "Description for food 10" },
  //   { id: 11, name: 'Food 12' , description: "Decription for food 11" },
  // ];

  var friends = []
  var friends = $http.get('http://localhost:8888/order/products/json')
    .success(function(data){
    	// console.log(data)
    	friends = data
      return {
	    all: function() {
	    	console.log(friends)
	      return friends;
	    },
	    get: function(friendId) {
	      // Simple index lookup
	      return friends[friendId];
	    }
	  }
    })
    .error(function(data, status, headers,config){
      console.log('data error');
    })


  // return {
  //   all: function() {
  //   	console.log(friends)
  //     return friends;
  //   },
  //   get: function(friendId) {
  //     // Simple index lookup
  //     return friends[friendId];
  //   }
  // }
});
