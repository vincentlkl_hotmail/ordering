<?php

class m140425_142847_create_orders extends CDbMigration
{
	public function up()
	{
		$this->createTable('orders', array(
            'id' => 'pk',
            'user_id' => 'integer',
            'status' => 'smallint',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',            
        ));
        $this->createIndex('index_orders_on_user_id', 'orders', 'user_id', false);
	}

	public function down()
	{
		echo "m140425_142847_create_orders does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}