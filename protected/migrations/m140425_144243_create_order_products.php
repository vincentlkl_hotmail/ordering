<?php

class m140425_144243_create_order_products extends CDbMigration
{
	public function up()
	{
		$this->createTable('order_products', array(
            'id' => 'pk',
            'order_id' => 'integer NOT NULL',
            'product_id' => 'integer NOT NULL',
            'quantity' => 'integer NOT NULL',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',            
        ));
        $this->createIndex('index_order_products_on_product_id', 'order_products', 'product_id', false);
        $this->createIndex('index_order_products_on_order_id', 'order_products', 'order_id', false);
	}

	public function down()
	{
		echo "m140425_144243_create_order_products does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}