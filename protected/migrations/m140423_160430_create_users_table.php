<?php

class m140423_160430_create_users_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('users', array(
            'id' => 'pk',
            'username' => 'string NOT NULL unique',
            'email' => 'string NOT NULL',
            'password' => 'string NOT NULL',
            'status' => 'smallint NOT NULL',
            'role' => 'smallint NOT NULL',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',            
        ));
	}

	public function down()
	{
		echo "m140423_160430_create_users_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}