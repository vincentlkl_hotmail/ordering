<?php

class m140425_145435_create_products extends CDbMigration
{
	public function up()
	{
		$this->createTable('products', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'price' => 'integer NOT NULL',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',            
        ));
	}

	public function down()
	{
		echo "m140425_145435_create_products does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}