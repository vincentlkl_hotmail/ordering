<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<?php /**
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	**/ ?>
	<!-- jquery library -->
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/order.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<header class="navbar-inverse bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" data-target=".bs-navbar-collapse" data-toggle="collapse">
        <span class="sr-only">
          Toggle navigation
        </span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo Yii::app()->baseUrl; ?>/">Ordering</a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" data-role="navigation">
      <ul class="nav navbar-nav">
        <?php if(Yii::app()->user->isGuest) { ?>
        	<li>
	          <a href="<?php echo Yii::app()->baseUrl; ?>/users/login">Login</a>
	      </li>	      
        <?php } else { ?> 
		<?php if(Yii::app()->user->isAdmin()){ ?>

			<li>
		          <a href="<?php echo Yii::app()->baseUrl; ?>/orders/admin">Orders</a>
		      </li>
			<li>
				<a href="<?php echo Yii::app()->baseUrl; ?>/products">Products</a>
			</li>
			
		<?php } else { ?>
			<li>
			<a href="<?php echo Yii::app()->baseUrl; ?>/orders">Orders</a>
			</li>
		<?php } ?>
			<li>
			<a href="<?php echo Yii::app()->baseUrl; ?>/users/logout">Logout (<?php echo Yii::app()->user->name; ?>)</a>
			</li>
        <?php } ?>
      </ul>
    </nav>
  </div>
</header>
<div class="container" id="page">

	<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
		<div class="alert alert-<?php echo $key; ?> alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $message; ?>
		</div>
	<?php } ?>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->

</body>
</html>
