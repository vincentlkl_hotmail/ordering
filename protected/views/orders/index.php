<?php
/* @var $this OrdersController */

$this->breadcrumbs=array(
	'Orders',
);
?>
<h1>Orders</h1>
<hr>
<div class="pull-right"><a href="<?php echo Yii::app()->baseUrl; ?>/orders/create" class="btn btn-primary">Create Order</a></div>
<br><br>
<table class="table table-bordered">
	<thead>
		<th>#</th>
		<th>Status</th>
		<th>Date</th>
	</thead>
	<?php foreach ($model as $i => $order) { ?>
		<tr>
			<td><a href="<?php echo Yii::app()->baseUrl; ?>/orders/<?php echo $order->id; ?>"><?php echo $order->id; ?></a></td>
			<td><?php echo $this->order_status($order->status); ?></td>
			<td><?php echo date('d-m-Y',strtotime($order->created_at)); ?></td>
		</tr>
	<?php } ?>	
</table>

<?php 
 $this->widget('CLinkPager', array(         
 	'pages' => $pages,         
 	'header' => '',         
 	'nextPageLabel' => 'Next',         
 	'prevPageLabel' => 'Prev',         
 	'selectedPageCssClass' => 'active',         
 	'htmlOptions' => array(             
 		'class' => 'pagination  pagination-sm',         
 	)))
?>
