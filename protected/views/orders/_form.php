<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-form',
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype' => 'multipart/form-data',
	),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>
	<?php echo CHtml::errorSummary($model); ?>
	<table class="table table-bordered">
		<thead>
			<th>#</th>
			<th>Product</th>
			<th>Price</th>
			<th>Quantity</th>
		</thead>
		<?php $index = 1; foreach ($products as $i => $product) { ?>
			<tr>
				<td><?php echo $product->id; ?></td>
				<td><?php echo $product->name; ?></td>
				<td><?php echo $product->price; ?></td>
				<td><?php echo $form->textField($order_products,'[' . $index . ']quantity',array(
							'size'=>60,
							'maxlength'=>255,
							'class'=>'form-control ', 
							'style'=>'width:200px;'
						)); ?>
					<?php echo $form->hiddenField($order_products,'[' . $index . ']product_id',array('value'=>$product->id)); ?>
				</td>
			</tr>
			<?php $index++; ?>
		<?php } ?>
	</table>

	<div class="form-group">
		<div class="controls">     
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Add Order' : 'Update Order',array('class'=>'btn btn-info')); ?>
			<a href="<?php echo Yii::app()->baseUrl; ?>/<?php echo Yii::app()->controller->id; ?>" class="btn btn-default">Cancel</a>
		</div>
	</div>

<?php $this->endWidget(); ?>  