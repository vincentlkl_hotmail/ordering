<h1>Order - (<?php echo date('d-m-y', strtotime($model->created_at)); ?>) <?php echo $this->order_status($model->status); ?></h1>
<hr>

<table class="table table-bordered">
	<thead>
		<th>#</th>
		<th>Product</th>
		<th>Unit Price</th>
		<th>Quantity</th>
		<th>Price</th>
	</thead>
	<?php $sum_price = array(); ?>
	<?php foreach ($model->order_products as $i => $op) { ?>
		<?php 
			$unit_price = $op->products->price; 
			$quantity = $op->quantity; 
			$price = $unit_price * $quantity;
			array_push($sum_price, $price);
		?>
		<tr>
			<td><?php echo $op->product_id; ?></td>
			<td><?php echo $op->products->name; ?></td>
			<td><?php echo $unit_price; ?></td>
			<td><?php echo $quantity; ?></td>	
			<td><?php echo $price; ?></td>		
		</tr>
	<?php } ?>	
	<tr>
		<td colspan="3"></td>
		<td><b>Total :</b></td>
		<td><?php echo array_sum($sum_price); ?></td>
	</tr>
</table>
