<?php
/* @var $this OrdersController */

$this->breadcrumbs=array(
	'Orders',
);
?>
<h1>Orders</h1>
<hr>
<br><br>
<table class="table table-bordered">
	<thead>
		<th>#</th>
		<th>Status</th>
		<th>User</th>
		<th>Date</th>
	</thead>
	<?php foreach ($model as $i => $order) { ?>
		<tr>
			<td><a href="<?php echo Yii::app()->baseUrl; ?>/orders/adminview/<?php echo $order->id; ?>"><?php echo $order->id; ?></a></td>
			<td><?php echo $this->order_status($order->status); ?></td>
			<td><?php echo $order->users->username; ?></td>
			<td><?php echo date('d-m-Y',strtotime($order->created_at)); ?></td>
		</tr>
	<?php } ?>	
</table>

<?php 
 $this->widget('CLinkPager', array(         
 	'pages' => $pages,         
 	'header' => '',         
 	'nextPageLabel' => 'Next',         
 	'prevPageLabel' => 'Prev',         
 	'selectedPageCssClass' => 'active',         
 	'htmlOptions' => array(             
 		'class' => 'pagination  pagination-sm',         
 	)))
?>
