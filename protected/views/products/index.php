<h1>Foods</h1>
<hr>
<div class="pull-right"><a class="btn btn-default" href="<?php echo Yii::app()->baseUrl; ?>/products/create"><span class="glyphicon glyphicon-plus"></span> Create Food</a></div>
<table class="table table-bordered">
	<thead>
		<th>#</th>
		<th>Food</th>
		<th>Price</th>
		<th>Created Date</th>
	</thead>
	<tbody>
		<?php foreach ($model as $i => $p) { ?>
			<tr>
				<td><a href="<?php echo Yii::app()->baseUrl; ?>/products/edit/<?php echo $p->id; ?>"><?php echo $p->id; ?></a></td>
				<td><?php echo $p->name; ?></td>
				<td><?php echo $p->price; ?></td>
				<td><?php echo $p->created_at; ?></td>
			</tr>
		<?php } ?>
	</tbody>

</table>


<?php 
 $this->widget('CLinkPager', array(         
	'pages' => $pages,         
	'header' => '',         
	'nextPageLabel' => 'Next',         
	'prevPageLabel' => 'Prev',         
	'selectedPageCssClass' => 'active',         
	'htmlOptions' => array(             
		'class' => 'pagination  pagination-sm',         
	)))
?>