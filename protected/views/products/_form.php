<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype' => 'multipart/form-data',
	),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<div class="form-group">
		<label for="project-price" class="col-sm-2 control-label">Product Name</label>
		<div class="col-sm-4">
			<?php echo $form->textField($model,'name',array(
				'size'=>60,
				'maxlength'=>255,
				'class'=>'form-control', 
			)); ?>    
		</div>
	</div>

	<div class="form-group">
		<label for="project-price" class="col-sm-2 control-label">Product Price</label>
		<div class="col-sm-4">
			<?php echo $form->textField($model,'price',array(
				'size'=>60,
				'maxlength'=>255,
				'class'=>'form-control', 
			)); ?>    
		</div>
	</div>

	<div class="form-group">
		<div class="controls">     
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create Product' : 'Update Product',array('class'=>'btn btn-info')); ?>
			<a href="<?php echo Yii::app()->baseUrl; ?>/<?php echo Yii::app()->controller->id; ?>" class="btn btn-default">Cancel</a>
		</div>
	</div>

<?php $this->endWidget(); ?>  