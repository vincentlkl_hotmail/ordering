<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype' => 'multipart/form-data',
	),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<?php echo CHtml::errorSummary($model); ?>
	<div class="form-group">
		<label for="project-price" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-10">
			<?php echo $form->textField($model,'username',array(
				'size'=>60,
				'maxlength'=>255,
				'class'=>'form-control', 
			)); ?>    
		</div>
	</div>

	<div class="form-group">
		<label for="project-price" class="col-sm-2 control-label">Email</label>
		<div class="col-sm-10">
			<?php echo $form->textField($model,'email',array(
				'size'=>60,
				'maxlength'=>255,
				'class'=>'form-control', 
			)); ?>    
		</div>
	</div>

	<div class="form-group">
		<label for="project-price" class="col-sm-2 control-label">Password</label>
		<div class="col-sm-10">
			<?php echo $form->passwordField($model,'password',array(
				'size'=>60,
				'maxlength'=>255,
				'class'=>'form-control', 
			)); ?>    
		</div>
	</div>

	<div class="form-group">
		<div class="controls">     
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Register' : 'Update Profile',array('class'=>'btn btn-info')); ?>
			<a href="<?php echo Yii::app()->baseUrl; ?>/" class="btn btn-default">Cancel</a>
		</div>
	</div>

<?php $this->endWidget(); ?>  