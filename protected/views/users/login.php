<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="col-sm-3"></div>
<div class="col-sm-6">
	<h1>Login</h1>

	<p>Please fill out the following form with your login credentials:</p>

	<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'htmlOptions'=>array(
			'class'=>'form-horizontal',
			'enctype' => 'multipart/form-data',
		),
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>

		<p class="note">Fields with <span class="required">*</span> are required.</p>

		<div class="row">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>

		<div class="row rememberMe">
			<?php echo $form->checkBox($model,'rememberMe'); ?>
			<?php echo $form->label($model,'rememberMe'); ?>
			<?php echo $form->error($model,'rememberMe'); ?>
		</div>

		<div class="row">
			<a href="<?php echo Yii::app()->baseUrl; ?>/users/registration">Register as a new user!</a>
		</div>

		<div class="row buttons">
			<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-default')); ?>
		</div>

	<?php $this->endWidget(); ?>
	</div><!-- form -->
</div>