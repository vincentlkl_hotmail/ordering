<?php

class ProductsController extends Controller
{
	
	public function actionJson()
	{

		// $model = Products::model()->findAll();
		// echo CJSON::encode(convertModelToArray($db));
		header('Content-type: application/json');

		$model = Products::model()->findAll();
		// echo 'angular.callbacks._0(';
		echo CJSON::encode($model);
		// echo ')';

	  Yii::app()->end();

	}


	public function actionIndex()
	{

		$criteria=new CDbCriteria();
		$criteria->order = 'name ASC';
		$count=Products::model()->count($criteria);
		$pages=new CPagination($count);

		// results per page
		$pages->pageSize=9;
		$pages->applyLimit($criteria);
		$model=Products::model()->findAll($criteria);	

		$this->render('index',array(
			'model'=>$model,
			'pages'=>$pages,
		));

	}

	public function actionCreate()
	{
		
		$model = new Products;

		if(isset($_POST['Products'])){
			$model->attributes=$_POST['Products'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "You have created this Product!");
				$this->redirect(Yii::app()->baseUrl.'/products');
			}
		}

		$this->render('create',array('model' => $model, ));
	}

	public function actionEdit($id)
	{
		
		$model = Products::model()->findByPk($id);
		if(isset($_POST['Products'])){
			$model->attributes=$_POST['Products'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "You have updated this Product!");
				$this->redirect(Yii::app()->baseUrl.'/products');
			}
		}

		$this->render('edit',array('model' => $model, ));
	}


}