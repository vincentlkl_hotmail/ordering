<?php

class OrdersController extends Controller
{
	public function actionIndex()
	{

		$criteria = new CDbCriteria();
		$criteria->order = 'created_at DESC';
		$criteria->condition = "user_id =:user_id ";
		$criteria->params = array(':user_id' => Yii::app()->user->id);
		$count = Orders::model()->count($criteria);
		$pages = new CPagination($count);

		// results per page
		$pages->pageSize=10;
		$pages->applyLimit($criteria);
		$model = Orders::model()->findAll($criteria);		

		$this->render('index', array(
				'model' => $model, 
				'pages'=>$pages,
		));
	}

	public function actionCreate()
	{
		$model = new Orders;
		$order_products = new OrderProducts;
		$products = Products::model()->findAll();

		if (isset($_POST['OrderProducts'])) {
			$model->status = 0;
			if($model->save()){
				foreach ($_POST['OrderProducts'] as $i => $order_product) {
					$order_products = new OrderProducts;
					$order_products->attributes = $_POST['OrderProducts'][$i];
					$order_products->order_id = $model->id;
					$order_products->save();
				}
				Yii::app()->user->setFlash('success', "You created an order!");
				$this->redirect(Yii::app()->baseUrl.'/orders');
			}
		}

		$this->render('create',array(
			'model' => $model, 
			'order_products' => $order_products,
			'products' => $products,
		));
	}

	public function actionView($id)
	{

		$model = Orders::model()->findByPk($id);

		$this->render('view', array(
				'model' => $model, 
		));
	}

	public function actionAdmin()
	{

		$criteria = new CDbCriteria();
		$criteria->order = 'created_at DESC';
		$count = Orders::model()->count($criteria);
		$pages = new CPagination($count);

		// results per page
		$pages->pageSize=10;
		$pages->applyLimit($criteria);
		$model = Orders::model()->findAll($criteria);		

		$this->render('admin', array(
				'model' => $model, 
				'pages'=>$pages,
		));
	}

	public function actionAdminView($id)
	{

		$model = Orders::model()->findByPk($id);
		if(isset($_POST['ready'])){
			$model->status = 1;
			if($model->save()){
				Yii::app()->user->setFlash('success', "You have confirmed an order!");
				$this->redirect(Yii::app()->baseUrl.'/orders/admin');
			}
		}

		$this->render('admin_view', array(
				'model' => $model, 
		));
	}

}