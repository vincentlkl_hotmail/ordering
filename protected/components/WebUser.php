<?php

class WebUser extends CWebUser
{
	// Store model to not repeat query.
	private $_model;
 
	// Return first name.
	// access it by Yii::app()->user->first_name
	function getFirst_Name(){
		$user = $this->loadUser(Yii::app()->user->id);
		return $user->first_name;
	}
 
	// This is a function that checks the field 'role'
	// in the User model to be equal to 1, that means it's admin
	// access it by Yii::app()->user->isAdmin()
	function isAdmin(){
		if(!Yii::app()->user->isGuest){
			$user = $this->loadUser(Yii::app()->user->id);
			if($user->status==0){
				Yii::app()->user->logout();
				Yii::app()->user->setFlash('danger', "You account is no longer active!");
			} else {
				return intval($user->role) == 1;
			}
		}
	}
 
	// Load user model.
	protected function loadUser($id=null)
	{
		if($this->_model===null)
		{
				if($id!==null)
						$this->_model=Users::model()->findByPk($id);
		}
		return $this->_model;
	}
}
?>
